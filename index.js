/*
* SPDX-FileCopyrightText: 2019 Opencontent SCARL
* SPDX-License-Identifier: GPL-2.0-only
*/

const express = require('express');
const bodyParser = require('body-parser');
const logger = require('./utils/logger');
const superagent = require('superagent');
require('dotenv').config();
const translate = require('./modules/translate');
const metrics = require('./modules/metrics');
const pjson = require('./package.json'); // Access to package.json

const app = express();

app.use(bodyParser.json());
app.use(
	bodyParser.urlencoded({
		extended: true,
	}),
);

// register of metrics
const register = metrics.registerMetrics();

// APIs
const status = require('./routes/status');

app.use('/status', status);

app.get('/metrics', function(req, res) {
	res.set('Content-Type', register.contentType);
	res.end(register.metrics());
});

app.listen(process.env.PORT || 8080, function() {
	logger.info(`OpenAgenda Translate ${pjson.version}`);
	logger.info(`GitLab Repository: ${pjson.repository.url}`);
	logger.info(
		`OpenAgenda Translate is running on port ${process.env.PORT || 8080}`);
	// Start daemon
	reRunTranslation(null);
});

/**
 * Starts translation's daemon
 * @param {String} lastTranslation: date from where to start translations
 */
function reRunTranslation(lastTranslation) {
	// Test connections
	superagent.get(`http://localhost:${process.env.PORT || 8080}/status`).
		end(function(err) {
			if (err) {
				logger.error({error: err});
			} else {
				// Daemon status log
				if (lastTranslation) {
					logger.info(
						`Starting translation\'s cycle from ${lastTranslation}...`,
					);
				} else logger.info(`Starting translation\'s cycle...`);

				// Start translation's cycle
				translate(metrics.custom_metrics, lastTranslation,
					function(err, date) {
						if (err) logger.error(err);
						if (date) {
							lastTranslation = date;
						}
						logger.info(
							`Sleeping for ` +
                    `${parseInt(process.env.SLEEP_INTERVAL) || 0} seconds...`,
						);
						setTimeout(function() {
							reRunTranslation(lastTranslation);
						}, parseInt(process.env.SLEEP_INTERVAL) * 1000 || 0);
					});
			}
		});
}

module.exports = app; // for testing
