# OpenAgenda Translate

Application for the automatic translation of a complete archive of [OpenAgenda](https://developers.italia.it/it/software/c_a116-opencontent-openagenda)

## Description

OpenAgenda Translate proposes to offer a service to automate the translation of the contents of the events present in an OpenAgenda archive thanks to the support of [Amazon Translate](https://aws.amazon.com/it/translate/).

The translation process can be customized by defining numerous configuration parameters such as, the contents to be translated, the languages ​​in which to carry out the translation, the maximum number of events to be translated or the date beyond which translations are to be made.

The default target languages are:
+ German (de)
+ Arabic (ar)
+ Turkish (tr)
+ Spanish (es)
+ Russian (ru)
+ Polish (pl)
+ French (fr)
+ Portuguese (pt)
+ Dutch (nl)
+ Chinese (Simplified) (zh)

To learn more about Amazon Translate and the ​​supported languages go to [What Is Amazon Translate?](https://docs.aws.amazon.com/en_us/translate/latest/dg/what-is.html)

## Requirements

System level requirements are defined within the `Dockerfile`, while application level requirements are defined within the `package.json` and `package-lock.json` files.

OpenAgenda Translate uses the AWS SDK to make translations: Valid credentials are required.

## Installation

To build and run the docker image execute the following commands

       docker-compose build
       docker-compose up

## How it Works

OpenAgenda Translate is designed to be run as a daemon that cyclically queries OpenAgenda APIs to get the whole event's archive and for each returned batch it starts the asynchronous translation of the single events.
The size of the batch, ie the number of events translated in parallel at each iteration can be customized through the OpenAgenda Translate configuration parameters (`BATCH_SIZE`). If not specified the default value is used.

At the end of each complete cycle the date is saved in order to avoid unnecessary request during subsequent iterations (See `START_DATE` to customize first-cycle starting point).

For test purposes it's possible to limit the maximum number of events to be translated, setting the `MAX_STEPS` variable.

If selected, the english translation is always the first one to be done, after which can follow the translation of all the other languages.
For every event the daemon checks if it has already been translated and, if it's not the case translates the requested fields, skips the event otherwise.
A clone of the original event data is then filled with the translated fields and posted back to the OpenAgenda API.

The translation process restarts after every execution; it's possible to set a time interval between consecutive restarts setting a `SLEEP_INTERVAL`.

Before starting the cycle the status of the application is checked, if wrong parameters are provided the translation will not start.

## API

OpenAgenda Translate implements two endpoints in which it exposes its status and metrics.

### Status

The `/status` endpoint exposes informations about connections to AWS Translate Service and OpenAgenda API.
If both connections are possible a status code `200` is returned, status code `400` otherwise, reporting which check has failed:

        400 Bad Request

        {
            "title":"Bad Request",
            "detail":"Connection refused",
            "status":400,
            "instance":"AWS Connection Refused"
        }

### Metrics

OpenAgenda Translate exposes information about ongoing translations on the `/metrics` endpoint.
The defined metrics are the following:

+ `daemon_translated_events_total`: Number of events translated
+ `daemon_translated_characters_total`: Number of characters translated
+ `daemon_error_total`: Number of errors encountered
+ `daemon_analyzed_total`: Number of events analyzed
+ `daemon_events_to_translate_total`: Number of events in the archive

Example:

        # HELP daemon_translated_events_total Number of translated events
        # TYPE daemon_translated_events_total counter

        # HELP daemon_translated_characters_total Number of translated characters
        # TYPE daemon_translated_characters_total counter

        # HELP daemon_error_total Number of errors
        # TYPE daemon_error_total counter

        # HELP daemon_analyzed_total Number of analyzed events
        # TYPE daemon_analyzed_total counter
        daemon_analyzed_total{language="en"} 6 1565178949470
        daemon_analyzed_total{language="de"} 6 1565178949471

        # HELP daemon_events_to_translate_total Number of events in the archive
        # TYPE daemon_events_to_translate_total gauge
        daemon_events_to_translate_total 229

## Environment

The environment variables required are the following:

|   Field                     |   Description                                                         | Default                             |
| --------------------------- | --------------------------------------------------------------------- | ----------------------------------- |
| `OPENAGENDA_HOSTNAME`       | OpenAgenda hostname where events are stored                           |   --                                |
| `OPENAGENDA_USERNAME`       | OpenAgenda Username for API Authentication                            |   --                                |
| `OPENAGENDA_PASSWORD`       | OpenAgenda Password for API Authentication                            |   --                                |
| `OPENAGENDA_API_VERSION`    | Version of the OpenAgenda APIs                                        |   v2                                |
| `ACCESS_KEY_ID`             | Access Key Id to connect to AWS                                       |   --                                |
| `SECRET_ACCESS_KEY`         | Secret Access Key to connect to AWS                                   |   --                                |
| `REGION`                    | AWS Region                                                            |   --                                |
| `AWS_API_VERSION`           | Version of the AWS APIs                                               |   2017-07-01                        |
| `BATCH_SIZE`                | Maximum page size returned from the OpenAgenga API                    |   10                                |
| `SLEEP_INTERVAL`            | Time spent between two complete  translations (s)                     |   0                                 |
| `MAX_STEPS`                 | Maximum number of Objects to translate  (for testing)                 |   --                                |
| `TARGET_LANGUAGES`          | List of all languages into which translate contents                   |   en de ar tr es ru pl fr pt nl zh  |
| `TARGET_FIELDS`             | List of all fields to translate                                       |   titolo short_title text abstract  |
| `LOG_LEVEL`                 | Logs level                                                            |   info                              |
| `LOG_FORMAT`                | Logs format (`json` or `text`)                                        |   json                              |
| `START_DATE`                | Initial date from which operate translation                           |   1990-1-1                          |

## LOGS

The OpenAgenda Translate logs report information on the status of the daemon: at the `info` level information and metrics are shown relating to the successfully translated events and the errors found, while at `debug` level the analyzed events that do not require translation are also highlighted

Logs format can be customized by choosing json or textual format while configuring environment variables.

Some examples are the following:

**Success:**

    info: {
            "id": 10396,
            "remoteId": "5e24d22fad2beb89bd82dc1559a8f907",
            "name": "Settimane Multisport",
            "target_language": "zh",
            "characters_count": 129,
            "data": {
              "titolo": "",
              "short_title": null,
              "image": {
                "filename": "Due-settimane-multisport-per-crescere-assieme_imagefull.jpg",
                "url": "http://openagenda.demo.opencontent.it/var/villedanaunia/storage/images/applicazioni/agenda/eventi/settimane-multisport/127219-1-ita-IT/Settimane-Multisport.jpg",
                "alt": "",
                "mime_type": "image/jpeg",
                "width": "850",
                "height": "638",
              },
              "abstract": "<p>Two weeks dedicated to sport to promote motor activity among girls and boys of the City of Anaunia</p>",
              "text": "",
              "file": null,
              "from_time": "2018-07-09T08:00:00+02:00",
              "to_time": "2018-07-20T16:30:00+02:00",
              "orario_svolgimento": "dalle 8.00 alle 16.30",
              "durata": null,
              "periodo_svolgimento": "dal lunedi al venerdi",
              "luogo_svolgimento": null,
              "geo": {
                "latitude": 46.328909,
                "longitude": 11.022972,
                "address": "Tuenno, Comunit&agrave; della Val di Non, TN, Trentino-Alto Adige, 38023, Italia",
              },
              "informazioni": "",
              "destinatari": null,
              "target": [
                "Giovani",
                "Bambini",
              ],
              "costi": null,
              "stato": null,
              "materia": [],
              "circoscrizione": [],
              "tipo_evento": [
                "Altro",
              ],
              "iniziativa": [],
              "associazione": [],
              "progressivo": null,
              "fonte": null,
              "images": [],
              "telefono": null,
              "fax": null,
              "email": null,
              "url": null,
              "rating": null,
              "argomento": [],
              "tipo_evento_relations": [],
            },
            "status_message": "Event translated successfully!",
          }

**Translation Already Exists:**

    debug: {
        "id": 8417,
        "remoteId": "41b8ec19a1efeafbc077c2f73a879c67",
        "name": "Tassullo, \"Formai dal Mont\"",
        "target_language": "en",
        "status_message": "Translation Already Exists!"
    }

**Errors:**

        error: {
            "id": 9415,
            "remoteId": "619fb05934f7550b78f2304635ff0840",
            "name": "Tuenno, film \"Trash\"",
            "target_language": "en",
            "characters_count": 0,
            "status_message": "An error occurred during translations",
            "error": {
                "error": {
                    "error": "Invalid Image",
                    "data": {
                        "filename": "cineforum-1-e1477728119467.jpg",
                        "url": "http://openagenda.demo.opencontent.it/var/villedanaunia/storage/images/comunicazione/informazione/eventi/tuenno-film-trash/107457-1-ita-IT/Tuenno-film-Trash.jpg",
                        "alt": "",
                        "mime_type": "image/jpeg",
                        "width": "600",
                        "height": "450"
                    }
                },
                "message": "Image url not valid: Image does not exists"
            }
        }

        error: {
            "id": 10230,
            "remoteId": "83a45db0779fef8da29b5cdc3be8d903",
            "name": "giornata ecologica",
            "target_language": "en",
            "characters_count": 0,
            "status_message": "An error occurred during translations",
            "error": {
                "error": {
                    "error": "Invalid File",
                    "data": {
                        "filename": "VdA_Format_Avvisi - giornata ecologica.pdf",
                        "url": "http://openagenda.demo.opencontent.it/content/download/10230/124235/2/VdA_Format_Avvisi - giornata ecologica.pdf"
                    }
                },
                "message": "File url not valid: File does not exists"
            }
        }

## Repository

    .
    ├── modules
    │   ├── aws.js                  # AWS Configuration
    │   ├── metrics.js              # Metrics definition
    │   └── translate.js            # Translator component
    ├── routes
    │   └── status.js               # /status endpoint manager
    ├── utils
    │   ├── json_response.js        # Responses builder
    │   ├── locale_codes.js         # Locales mapping
    │   └── logger.js               # Logger Configuration
    ├── .dockerignore
    ├── .eslintrc.json              # Eslint Configuration
    ├── .gitignore
    ├── .gitlab-ci.yml              # CI
    ├── docker.compose.yml
    ├── Dockerfile
    ├── index.js                    # Main component
    ├── LICENSE
    ├── package.json
    ├── package-lock.json
    ├── publiccode.yml
    ├── README
    └── template.env               # template for configuration variables

The base folder contains the following files:

    index.js

contains the source code to start the server

    package.json
    packege-lock.json

define the application dependencies

    Dockerfile
    docker-compose.yml

files needed to build the docker image and run the container.

    template.env

is a template for the OpenAgenda Translate variable configuration file.

    /modules

contains modules for the configuration of AWS, for the definition of customized metrics and the main module that deals with the translation of the contents of the events.

    /routes

contains the files necessary for defining the OpenAgenda Translate endpoints.

    /utils

contains utility files for locales mapping, logging, building and sending json responses.

## Project Status

The status of the project is stable, for the knows issues please refer to the issues section of the Project

## Continuous Integration

The software has a CI pipeline, which executes a build at each commit and performs a check of the syntactical correctness of the source code, reformatting the code if necessary.

Docker images are available in the [GitLab Registry](https://gitlab.com/opencontent/openagenda-translate/container_registry).

The CI pipeline is available at the following url:

[https://gitlab.com/opencontent/openagenda-translate/pipelines](https://gitlab.com/opencontent/babelfish-daemon/pipelines)

## Copyright

The owner of this software is [Opencontent SCARL](https://www.opencontent.it).

The software is released under license `GPL-2.0-only`

## Security

Please send security alerts **privately** to [info@opencontent.it](mailto:info@opencontent.it).
