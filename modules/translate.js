/*
* SPDX-FileCopyrightText: 2019 Opencontent SCARL
* SPDX-License-Identifier: GPL-2.0-only
*/

const superagent = require('superagent');
const waterfall = require('async-waterfall');
const querystring = require('querystring');
const AWS = require('./aws');
const localeCode = require('../utils/locale_codes');
const logger = require('../utils/logger');
require('dotenv').config();

// Configuring Defaults
const BATCH_SIZE = parseInt(process.env.BATCH_SIZE) || 10;
console.log(BATCH_SIZE);
const MAX_STEPS = parseInt(process.env.MAX_STEPS);
const TARGET_LANGUAGES = process.env.TARGET_LANGUAGES
	? process.env.TARGET_LANGUAGES.trim().split(' ')
	: ['de', 'ar', 'tr', 'es', 'ru', 'pl', 'fr', 'pt', 'nl', 'zh'];
clearTargetLanguages(TARGET_LANGUAGES);
const TARGET_FIELDS = process.env.TARGET_FIELDS
	? process.env.TARGET_FIELDS.trim().split(' ')
	: ['titolo', 'short_title', 'text', 'abstract'];

// Metrics: Values will be set on startTranslation call
let daemonAnalyzedTotal;
let daemonTranslatedEventsTotal;
let daemonErrorTotal;
let daemonTranslatedCharactersTotal;
let daemonEventsToTranslateTotal;

const awsTranslate = AWS.initTranslate(); // Init AWS Translate Service
let analyzedEventsCounter; // Number of analyzed
let completedEventsCounter; // Number of completed events

/**
 * Starts the events translation
 * @param {Object} metricsDict: dictionary of the exposed metrics
 * @param {String} lastTranslation: date of the last massive translation
 * @param {function} callback: function called when results are available
 */
function startTranslation(metricsDict, lastTranslation, callback) {
	// Set metrics
	daemonAnalyzedTotal = metricsDict.daemon_analyzed_total;
	daemonTranslatedEventsTotal = metricsDict.daemon_translated_events_total;
	daemonErrorTotal = metricsDict.daemon_error_total;
	daemonTranslatedCharactersTotal =
      metricsDict.daemon_translated_characters_total;
	daemonEventsToTranslateTotal =
      metricsDict.daemon_events_to_translate_total;

	if (
		!daemonAnalyzedTotal ||
      !daemonTranslatedEventsTotal ||
      !daemonErrorTotal ||
      !daemonTranslatedCharactersTotal ||
      !daemonEventsToTranslateTotal
	) {
		callback('Metrics are Mandatory Parameters', null);
		return;
	}

	// OpenAgenda API Query construction
	let lastModified = lastTranslation || process.env.START_DATE;
	// Date of the last translation
	lastModified = lastModified || new Date('1990-1-1').toISOString();

	let query = `classes [event] and limit ${BATCH_SIZE} sort [modified => asc]`;
	query = query.concat(` and modified range [${lastModified}, *]`);

	logger.debug({
		querystring: query,
	});

	// Initialize counter
	analyzedEventsCounter = 0;
	completedEventsCounter = 0;

	getEvents(
		`http://${process.env.OPENAGENDA_HOSTNAME}/api/opendata/${process.env.OPENAGENDA_API_VERSION ||
      'v2'}/content/search/${querystring.escape(
			query,
		)}`,
		callback,
	);
}

/**
 * Split text if longer than specified length
 * @param {String} text: Text to split
 * @param {int} maxLength: Maximum length of the text beyond which to perform
 * the splitting
 * @return {[]}: Array of splitted strings
 */
function splitText(text, maxLength) {
	const subTexts = [];
	while (text.length > maxLength) {
		// Index of the last point before max_length
		let index = text.substring(0, maxLength).lastIndexOf('. ');
		index = index <= 0 ? maxLength : index;
		subTexts.push(text.substring(0, index));
		let i = text.indexOf('. ', index) + 1;
		if (i < index || i > index + maxLength) i = index;
		text = text.substring(i);
	}
	subTexts.push(text);
	return subTexts;
}

/**
 * Checks if input target languages are supported, if it is not the case it will
 * be removed.
 * @param {String[]} targetLanguages: Array of target languages
 */
function clearTargetLanguages(targetLanguages) {
	const toDelete = []; // Save index to delete
	targetLanguages.forEach(function(target, index) {
		// If entity is not contained in allowed entities save index to be deleted
		if (!localeCode[target]) {
			logger.warn(`"${target}" is not supported!`);
			toDelete.push(index);
		}
	});
	// Delete not supported entities
	while (toDelete.length > 0) {
		const index = toDelete.pop();
		targetLanguages.splice(index, 1);
	}
	logger.debug(`Purged entities are: ${targetLanguages}`);
}

/**
 * Queries the OpenAgenda API to retrieve all the events and translates them
 * @param {string} url: OpenAgenda API endpoint for events retrieval
 * @param {function} callback: function called when results are available
 */
function getEvents(url, callback) {
	logger.info({
		url,
	});

	// GET events
	superagent.get(url).end(function(err, res) {
		if (err) {
			logger.error({
				error_message: 'An error occurred while fetching events',
				error: err,
			});
		} else {
			// Next Page Url
			const totalCount = parseInt(res.body.totalCount);
			const nextPage = res.body.nextPageQuery;

			daemonEventsToTranslateTotal.set(totalCount);

			// No events to translate
			if (totalCount === 0) {
				logger.info('No events to translate');
				callback(null, new Date().toISOString());
				return;
			}

			// Iterate through events and start translations
			waterfall([
				function translatePage(done) {
					res.body.searchHits.forEach(function(event) {
						// Get event by RemoteId: This additional request prevents useless
						// translations when API responds with different events
						// http://openagenda.demo.opencontent.it/api/opendata/v2/content/read/17efcd74cd2c0c332fc9d8e9ad5cfbf8
						// http://openagenda.demo.opencontent.it/api/opendata/v2/content/search/classes%20%5Bevent%5D%20and%20limit%201
						const eventUrl = `http://${
							process.env.OPENAGENDA_HOSTNAME
						}/api/opendata/${process.env.OPENAGENDA_API_VERSION ||
            'v2'}/content/read/${event.metadata.remoteId}`;

						// GET Event by remoteId
						superagent.get(eventUrl).end(function(err, res) {
							if (err) {
								done(err, event);
							} else {
								event = res.body;
								analyzedEventsCounter += 1;
								// If analyzed has not reached cap or MAX_STEPS is not defined
								// go next, translate the event otherwise
								if (!MAX_STEPS || analyzedEventsCounter <= MAX_STEPS) {
									waterfall(
										[
											// English Translation First
											function translateEnglish(next) {
												if (TARGET_LANGUAGES.includes('en')) {
													translate(event, 'en', function(err, event) {
														if (err) {
															// Set event as completed
															completedEventsCounter += 1;
															next(err, null);
														} else {
															// Go to next translations
															next(null, event);
														}
													});
												} else {
													next(null, event);
												}
											},
											function translateOthers(event, next) {
												// Counter for completed single event translation
												let numCompleted = 0;
												// Other translations
												TARGET_LANGUAGES.forEach(function(target) {
													translate(event, target, function() {
														numCompleted += 1;
														// If all translations are completed increment
														// number of completed events
														if (numCompleted === TARGET_LANGUAGES.length) {
															completedEventsCounter += 1;
															next(null, event);
														}
													});
												});
											},
										], function() {
											// If all events in the page have been processed
											// request next page
											if (completedEventsCounter % BATCH_SIZE === 0 ||
                            completedEventsCounter === totalCount) {
												logger.debug('Batch completed');
												// Do not request next page if cap has reached
												// at the end of the batch
												if (MAX_STEPS && MAX_STEPS ===
                              analyzedEventsCounter) {
													logger.debug('Limit reached');
													done(null, null);
												} else {
													done(null, nextPage);
												}
											}
										});
								} else {
									// MAX_STEPS is defined and limit has been reached
									// For the first event that reaches cap go next
									if (analyzedEventsCounter === MAX_STEPS + 1) {
										logger.debug('Limit reached');
										done(null, null);
									}
								}
							}
						});
					});
				},
				// Request next page
				function getNextPage(url) {
					if (url) {
						logger.debug('next page requested ' + url);
						getEvents(url, callback);
					} else {
						logger.debug('No more events to analyze');
						logger.info('Translation Completed');
						// Save date of the last Massive Translation is MAX_STEPS undefined
						if (!MAX_STEPS) {
							logger.debug('Save date of the last massive translation');
							callback(null, new Date().toISOString());
						} else {
							callback(null, null);
						}
					}
				},
			]);
		}
	});
}

/**
 * Translates a single event
 * @param {Object} event
 * @param {string} targetLanguage
 * @param {function} callback: function called when results are available
 */
function translate(event, targetLanguage, callback) {
	// Parameters check
	if (!event || !targetLanguage) {
		callback('Invalid Parameters: Missing event or target language', null);
		return;
	}
	daemonAnalyzedTotal.inc({language: targetLanguage}, 1, Date.now());

	let charCount = 0; // Init characters counter

	// If exists use english version to translate
	let sourceLanguage = 'it';
	if (event.data[localeCode.en]) {
		sourceLanguage = 'en';
	}
	// Translation Already Exists
	if (event.data[localeCode[targetLanguage]]) {
		logger.debug({
			id: event.metadata.id,
			remoteId: event.metadata.remoteId,
			name: event.metadata.name['ita-IT'],
			target_language: targetLanguage,
			status_message: 'Translation Already Exists!',
		});
		callback(null, event);
		return;
	}
	// Start translation
	waterfall(
		[
			// Check if image exists
			function checkImage(done) {
				if (event.data['ita-IT'].image) {
					if (
						!event.data['ita-IT'].image.url.startsWith(
							`http://${process.env.OPENAGENDA_HOSTNAME}`,
						)
					) {
						event.data['ita-IT'].image.url = `http://${
							process.env.OPENAGENDA_HOSTNAME
						}${event.data['ita-IT'].image.url}`;
					}
					superagent.get(event.data['ita-IT'].image.url).end(function(err) {
						if (err) {
							done(
								{
									error: {
										error: 'Invalid Image',
										data: event.data['ita-IT'].image,
									},
									message: 'Image url not valid: Image does not exists',
								},
								event,
							);
						} else {
							done(null, event);
						}
					});
				} else {
					done(null, event);
				}
			},
			// Check if file exists
			function checkFile(event, done) {
				if (event.data['ita-IT'].file) {
					superagent.get(event.data['ita-IT'].file.url).end(function(err) {
						if (err) {
							done(
								{
									error: {
										error: 'Invalid File',
										data: event.data['ita-IT'].file,
									},
									message: 'File url not valid: File does not exists',
								},
								event,
							);
						} else {
							done(null, event);
						}
					});
				} else {
					done(null, event);
				}
			},
			// Initialization of translations dictionary
			function initTranslationsDict(event, done) {
				const translations = {};
				TARGET_FIELDS.forEach(function(field, index) {
					// Only non null fields will be translated
					if (event.data[localeCode[sourceLanguage]][field]) {
						translations[field] = '';
					}
					if (index === TARGET_FIELDS.length - 1) {
						done(null, translations);
					}
				});
			},
			// Fields Translations
			function translateFields(translations, done) {
				let fieldsCompleted = 0;
				const fields = Object.keys(translations);
				fields.forEach(function(field) {
					const subTexts = splitText(
						event.data[localeCode[sourceLanguage]][field],
						5000,
					);
					let numTranslations = 0;
					subTexts.forEach(function(text, subTextIndex) {
						const params = {
							SourceLanguageCode: 'it',
							TargetLanguageCode: 'en',
							Text: text,
						};
						awsTranslate.translateText(params, function(err, data) {
							if (err) {
								done(err, event);
							} else {
								subTexts[subTextIndex] = data.TranslatedText;
								numTranslations += 1;
								// Translation completed
								if (numTranslations === subTexts.length) {
									fieldsCompleted += 1;
									daemonTranslatedCharactersTotal.inc(
										{language: targetLanguage},
										event.data[localeCode[sourceLanguage]][field].length,
										Date.now(),
									);
									// Join translated sub strings
									translations[field] = subTexts.join('. ');
									// Count translated characters
									charCount +=
											event.data[localeCode[sourceLanguage]][field].length;
									// Last field
									console.log(fieldsCompleted, fields.length);
									if (fieldsCompleted === fields.length) {
										done(null, translations);
									}
								}
							}
						});
					});
				});
			},
			// Clone data
			function createEvent(translations, done) {
				const newData = Object.assign({}, event.data['ita-IT']);
				const fields = Object.keys(translations);
				// Fill with translations
				fields.forEach(function(field) {
					newData[field] = translations[field];
				});
				// update language field
				event.data[localeCode[targetLanguage]] = newData;
				done(null, event);
			},
			// POST event
			function postEvent(event, done) {
				// Payload
				const data = {
					metadata: {
						remoteId: event.metadata.remoteId,
						languages: [localeCode[targetLanguage]],
					},
					data: {},
				};
				data.data[localeCode[targetLanguage]] =
              event.data[localeCode[targetLanguage]];

				superagent.post(
					`http://${process.env.OPENAGENDA_HOSTNAME}/api/opendata/${process.env.OPENAGENDA_API_VERSION ||
              'v2'}/content/update`,
				).auth(
					process.env.OPENAGENDA_USERNAME,
					process.env.OPENAGENDA_PASSWORD,
				).send(data).end(function(err) {
					if (err) {
						done(err, event);
					} else {
						done(null, event);
					}
				});
			},
		],
		// Logs
		function(err, event) {
			if (err) {
				daemonErrorTotal.inc({language: targetLanguage}, 1, Date.now());
				logger.error({
					id: event.metadata.id,
					remoteId: event.metadata.remoteId,
					name: event.metadata.name['ita-IT'],
					target_language: targetLanguage,
					characters_count: charCount,
					data: event.data[localeCode[targetLanguage]],
					status_message: 'An error occurred during translations',
					error: err,
				});
				callback(err, null);
			} else {
				daemonTranslatedEventsTotal.inc(
					{language: targetLanguage},
					1,
					Date.now(),
				);
				logger.info({
					id: event.metadata.id,
					remoteId: event.metadata.remoteId,
					name: event.metadata.name['ita-IT'],
					target_language: targetLanguage,
					characters_count: charCount,
					data: event.data[localeCode[targetLanguage]],
					status_message: 'Event translated successfully!',
				});
				callback(null, event);
			}
		},
	);
}

module.exports = startTranslation;
