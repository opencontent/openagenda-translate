/*
* SPDX-FileCopyrightText: 2019 Opencontent SCARL
* SPDX-License-Identifier: GPL-2.0-only
*/

const AWS = require('aws-sdk');
require('dotenv').config();

/**
 * Configures AWS.Translate Object
 * @return {Translate}
 */
function initAWSTranslate() {
	return new AWS.Translate({
		accessKeyId: process.env.ACCESS_KEY_ID,
		secretAccessKey: process.env.SECRET_ACCESS_KEY,
		region: process.env.REGION,
		apiVersion: process.env.AWS_API_VERSION || '2017-07-01',
	});
}

/**
 * Checks whether an AWS connection is possible
 * @param {Object} awsTranslate: AWSTranslate instance
 * @param {function} callback: function called when results are available
 */
function checkConnection(awsTranslate, callback) {
	// Check configuration variables existence
	if (
		!process.env.ACCESS_KEY_ID ||
      !process.env.SECRET_ACCESS_KEY ||
      !process.env.REGION
	) {
		callback('AWS Environment Parameters are Mandatory Fields', null);
		return;
	}
	// Test request to AWS Translate
	awsTranslate.listTerminologies(function(err, res) {
		if (err) {
			callback(err, null);
		} else {
			callback(null, res);
		}
	});
}

module.exports = {
	initTranslate: initAWSTranslate,
	checkConnection,
};
