/*
* SPDX-FileCopyrightText: 2019 Opencontent SCARL
* SPDX-License-Identifier: GPL-2.0-only
*/

const client = require('prom-client');

// Custom metrics definition
const daemonAnalyzedTotal = new client.Counter({
	name: 'daemon_analyzed_total',
	help: 'Number of analyzed events',
	labelNames: ['language'],
});

const daemonTranslatedEventsTotal = new client.Counter({
	name: 'daemon_translated_events_total',
	help: 'Number of translated events',
	labelNames: ['language'],
});

const daemonErrorTotal = new client.Counter({
	name: 'daemon_error_total',
	help: 'Number of errors',
	labelNames: ['language'],
});

const daemonTranslatedCharactersTotal = new client.Counter({
	name: 'daemon_translated_characters_total',
	help: 'Number of translated characters',
	labelNames: ['language'],
});

const daemonEventsToTranslateTotal = new client.Gauge({
	name: 'daemon_events_to_translate_total',
	help: 'Number of events in the archive',
});

/**
 * Registers custom metrics
 * @return {Registry} Resister where metrics are registered
 */
function registerMetrics() {
	const Registry = client.Registry;
	const register = new Registry();

	register.registerMetric(daemonTranslatedEventsTotal);
	register.registerMetric(daemonTranslatedCharactersTotal);
	register.registerMetric(daemonErrorTotal);
	register.registerMetric(daemonAnalyzedTotal);
	register.registerMetric(daemonEventsToTranslateTotal);

	return register;
}

module.exports = {
	registerMetrics,
	custom_metrics: {
		daemon_translated_events_total: daemonTranslatedEventsTotal,
		daemon_translated_characters_total: daemonTranslatedCharactersTotal,
		daemon_error_total: daemonErrorTotal,
		daemon_analyzed_total: daemonAnalyzedTotal,
		daemon_events_to_translate_total: daemonEventsToTranslateTotal,
	},
};
