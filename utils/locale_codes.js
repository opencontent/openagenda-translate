/*
* SPDX-FileCopyrightText: 2019 Opencontent SCARL
* SPDX-License-Identifier: GPL-2.0-only
*/

// https://doc.ez.no/eZ-Publish/Technical-manual/4.4/Features/Multi-language/Configuring-your-site-locale

// Distionary for locales mapping
const localeCodes = {
	// Arabic ar
	'ar': 'ara-SA',
	// Chinese (Simplified) zh
	'zh': 'chi-CN',
	// Chinese (Traditional) zh-TW
	'zh-TW': 'chi-TW',
	// Czech cs
	'cs': 'cze-CZ',
	// Danish da
	'da': 'dan-DK',
	// Dutch nl
	'nl': 'dut-NL',
	// English en
	'en': 'eng-GB',
	// Finnish fi
	'fi': 'fin-FI',
	// French	fr
	'fr': 'fre-FR',
	// German	de
	'de': 'ger-DE',
	// Hebrew	he
	'he': 'heb-IL',
	// Hindi	hi
	'hi': 'hin-IN',
	// Indonesian	id
	'id': 'ind-ID',
	// Italian	it
	'it': 'ita-IT',
	// Japanese	ja
	'ja': 'jpn-JP',
	// Korean	ko
	'ko': 'kor-KR',
	// Norwegian	no
	'no': 'nor-NO',
	// Polish	pl
	'pl': 'pol-PL',
	// Portuguese	pt
	'pt': 'por-PT',
	// Russian	ru
	'ru': 'rus-RU',
	// Spanish	es
	'es': 'esl-ES',
	// Swedish	sv
	'sv': 'swe-SE',
	// Turkish	tr
	'tr': 'tur-TR',
	// Persian	fa NONE
	// Malay	ms NONE
};

module.exports = localeCodes;
