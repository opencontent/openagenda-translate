/*
* SPDX-FileCopyrightText: 2019 Opencontent SCARL
* SPDX-License-Identifier: GPL-2.0-only
*/

const winston = require('winston');
require('dotenv').config();

// Builds json logs
const prettyJson = winston.format.printf(function(info) {
	if (info.message.constructor === Object) {
		info.message = JSON.stringify(info.message, null, 4);
	}
	return `${info.level}: ${info.message}`;
});

// Builds text logs
const prettyString = winston.format.printf(function(info) {
	if (info.message.constructor === Object) {
		let message = '\n';
		// Create formatted string from object
		Object.keys(info.message).forEach(function(key) {
			message = message.concat(
				`${key}: ${JSON.stringify(info.message[key], null, 4)}\n`
			);
		});
		info.message = message;
	}
	return `${info.level}: ${info.message}`;
});

let error = false;
let logger;

let format = process.env.LOG_FORMAT || 'json';
// Invalid logs format: changed to default
if (format !== 'json' && format !== 'text') {
	error = true;
	format = 'json';
}

if (format === 'json') {
	logger = winston.createLogger({
		level: process.env.LOG_LEVEL || 'info',
		format: winston.format.combine(
			winston.format.colorize(),
			winston.format.prettyPrint(),
			winston.format.simple(),
			prettyJson,
		),
		transports: [new winston.transports.Console()],
	});
	if (error) {
		logger.error('Invalid logs format: Changed to JSON');
	}
} else {
	logger = winston.createLogger({
		level: process.env.LOG_LEVEL || 'info',
		format: winston.format.combine(
			winston.format.colorize(),
			winston.format.prettyPrint(),
			winston.format.simple(),
			prettyString,
		),
		transports: [new winston.transports.Console()],
	});
}

module.exports = logger;
