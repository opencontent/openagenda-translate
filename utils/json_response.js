/*
* SPDX-FileCopyrightText: 2019 Opencontent SCARL
* SPDX-License-Identifier: GPL-2.0-only
*/

module.exports = function(res, detail, instance, statusCode) {
	statusCode = parseInt(statusCode, 10);
	let title = '';

	switch (statusCode) {
	case 200:
		title = 'OK';
		break;
	case 400:
		title = 'Bad Request';
		break;
	default:
		throw Error('Invalid status code');
	}

	// Enabling CORS
	res.header('Access-Control-Allow-Origin', '*');
	// Support header x-access-token for the authentication token
	res.header(
		'Access-Control-Allow-Headers',
		'Origin, X-Requested-With, Content-Type, Accept, ' +
      'x-access-token, Authorization',
	);
	res.header('Content-Type', 'application/json');

	if (statusCode === 200) {
		res.status(statusCode).json(instance);
	} else {
		res.status(statusCode).send({
			title,
			detail,
			status: statusCode,
			instance,
		});
	}
};
