/*
* SPDX-FileCopyrightText: 2019 Opencontent SCARL
* SPDX-License-Identifier: GPL-2.0-only
*/

const express = require('express');
const bodyParser = require('body-parser');
const aws = require('../modules/aws');
const superagent = require('superagent');
const waterfall = require('async-waterfall');

const router = new express.Router();
const response = require('../utils/json_response');
require('dotenv').config();

router.use(bodyParser.json());

const awsTranslate = aws.initTranslate();

router.route('/')
// listConsents: list consents registered
	.get(function(req, res) {
		waterfall(
			[
				// Test aws connection
				function awsConnection(done) {
					aws.checkConnection(awsTranslate, function(err, resp) {
						if (err) {
							done(
								{
									error: 'AWS Connection Refused',
									message: err.message,
								},
								null,
							);
						} else {
							done(null, resp);
						}
					});
				},
				// Test openAgenga API Connection
				function openAgendaConnection(resp, done) {
					if (
						!process.env.OPENAGENDA_HOSTNAME ||
									!process.env.OPENAGENDA_USERNAME ||
									!process.env.OPENAGENDA_PASSWORD
					) {
						done(
							{
								error: 'OpenAgenda Connection Refused',
								message:
													'OpenAgenda Hostname, Username and Password are ' +
													'mandatory fields',
							},
							null,
						);
					} else {
						superagent.get(
							`http://${
								process.env.OPENAGENDA_HOSTNAME
							}/api/opendata/${process.env.OPENAGENDA_API_VERSION ||
										'v2'}/classes/event`,
						).auth(
							process.env.OPENAGENDA_USERNAME,
							process.env.OPENAGENDA_PASSWORD,
						).end(function(err, resp) {
							if (err) {
								done(
									{
										err: 'OpenAgenda Connection Refused',
										message: err.message,
									},
									null,
								);
							} else {
								done(null, resp);
							}
						});
					}
				},
			],
			function(err) {
				if (err) {
					response(res, 'Connection refused', err, 400);
				} else {
					response(res, 'OK', 'Connection Established!', 200);
				}
			},
		);
	});

module.exports = router;
